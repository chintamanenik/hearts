//
//  Deck.cpp
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/9/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#include "Deck.h"

#include "Random.h"


PlayingCard Deck::GetRandomCard() {
    int index = Random::Number(0, Size() - 1);
    PlayingCard card = deck[index];
    deck.erase(deck.begin() + index);
    return card;
}

int Deck::Size() const {
    return static_cast<int>(deck.size());
}

Deck::Deck() {
    for (enum PlayingCard::Suit suit : PlayingCard::suitCollection) {
        for(enum PlayingCard::Rank rank : PlayingCard::rankCollection){
            PlayingCard card(rank,suit);
            deck.push_back(card);
        }
    }
}
