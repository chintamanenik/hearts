//
//  HeartsPlayer.h
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/12/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#ifndef __Hearts__HeartsPlayer__
#define __Hearts__HeartsPlayer__

#include <vector>

#include "PlayingCard.h"

class HeartsPlayer{
public:
    HeartsPlayer();
    
    std::vector<PlayingCard> PassCards();
    PlayingCard PromptCard();
    void AddCards(const std::vector<PlayingCard>&);
    std::vector<PlayingCard> Hand() const;
    bool IsFirstPlayer() const;
    bool IsHandEmpty() const;
    void PrintHand() const;
private:
    std::vector<PlayingCard> hand;
};

#endif /* defined(__Hearts__HeartsPlayer__) */
