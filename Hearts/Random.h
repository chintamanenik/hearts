//
//  Random.h
//  Hearts
//
//  Wrapper for a random number generator algorithm
//
//  Created by Kundan Chintamaneni on 2/9/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#ifndef __Hearts__Random__
#define __Hearts__Random__

#include <random>
#include <map>
#include <utility>

class Random {
public:
    static int Number(int min, int max);
private:
    using RNG = std::mt19937; //algorithm used for random number generation
    using num_type = unsigned int; //numbertype used to store seed

    static RNG& Generator();
    
    static RNG engine;
    static bool seeded;
};

#endif /* defined(__Hearts__Random__) */
