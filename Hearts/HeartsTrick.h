//
//  HeartsTrick.h
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/14/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#ifndef __Hearts__HeartsTrick__
#define __Hearts__HeartsTrick__

#include <vector>

#include "PlayingCard.h"
#include "HeartsPlayer.h"

class HeartsTrick {
public:
    HeartsTrick(bool isHeartsBroken, bool isFirstTrick);
    
    PlayingCard getCard(const HeartsPlayer* player) const;
    bool IsHeartsBroken() const;
    bool IsCompleted() const;
    bool IsValid(const PlayingCard& card, const std::vector<PlayingCard>& hand) const;
    void AddCard(const PlayingCard& card, HeartsPlayer* player);
    HeartsPlayer* Winner() const;
    void Print();
private:
    const static int NUM_PLAYERS;
    
    static int RankValue(const enum PlayingCard::Rank rank);
    
    std::vector<PlayingCard> cards;
    std::vector<HeartsPlayer*> players;
    bool isHeartsBroken;
    bool isFirstTrick;
    enum PlayingCard::Suit baseSuit;
};

#endif /* defined(__Hearts__HeartsTrick__) */
