//
//  HeartsPlayer.cpp
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/12/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#include <iostream>

#include "HeartsPlayer.h"
#include "PlayingCard.h"
#include "Random.h"

HeartsPlayer::HeartsPlayer() : hand(std::vector<PlayingCard>()) {
    
}

bool HeartsPlayer::IsFirstPlayer() const {
    for(PlayingCard card : hand){
        if ((card.Rank() == PlayingCard::Rank::TWO) && (card.Suit() == PlayingCard::Suit::CLUBS)) {
            return true;
        }
    }
    return false;
}

void HeartsPlayer::AddCards(const std::vector<PlayingCard>& cards) {
    for (PlayingCard card: cards) {
        hand.push_back(card);
    }
}

std::vector<PlayingCard> HeartsPlayer::Hand() const {
    return hand;
}

void HeartsPlayer::PrintHand() const {
    for (PlayingCard card : hand) {
        std::cout << card.ToString();
        std::cout << " ";
    }
    std::cout << "\n";
}

bool HeartsPlayer::IsHandEmpty() const {
    return hand.empty();
}

std::vector<PlayingCard> HeartsPlayer::PassCards() {
    std::vector<PlayingCard> cards;
    cards.push_back(PromptCard());
    cards.push_back(PromptCard());
    cards.push_back(PromptCard());
    return cards;
}

PlayingCard HeartsPlayer::PromptCard() {
    int size = static_cast<int>(hand.size());
    int index = Random::Number(0, size-1);
    PlayingCard card = hand[index];
    hand.erase(hand.begin() + index);
    return card;
}




