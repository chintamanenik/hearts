//
//  HeartsGame.h
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/12/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#ifndef __Hearts__HeartsGame__
#define __Hearts__HeartsGame__

#include <vector>

#include "HeartsPlayer.h"
#include "HeartsTrick.h"
#include "HeartsEngine.h"

class HeartsGame {
public:
    HeartsGame(const std::vector<HeartsPlayer*>& players, HeartsEngine::PassScheme passScheme);
    
    int Score(const HeartsPlayer* player) const;
    void Begin();
private:
    HeartsPlayer* NextPlayer(const HeartsPlayer* player) const;
    void TakeTrick();
    void PassCards(const HeartsEngine::PassScheme passScheme);
    bool HasShotMoon(const HeartsPlayer* player) const;

    std::vector<HeartsPlayer*> players;
    std::vector<HeartsTrick> tricks;
    HeartsPlayer* trickLeader;
    bool isFirstTrick;
    bool isHeartsBroken;
    HeartsEngine::PassScheme passScheme;
    
    const int HEARTS_PENALTY;
    const int QUEEN_SPADES_PENALTY;
};

#endif /* defined(__Hearts__HeartsGame__) */
