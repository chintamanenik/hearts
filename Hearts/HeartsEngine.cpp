//
//  HeartsEngine.cpp
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/12/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#include "HeartsEngine.h"

#include <iostream>

#include "HeartsGame.h"

HeartsEngine::HeartsEngine(const std::vector<HeartsPlayer*>& players) : players(players), passScheme(PassScheme::LEFT), scores(), MAX_SCORE(100) {
    for (HeartsPlayer* player : players) {
        scores.insert(std::pair<HeartsPlayer*, int>(player, 0));
    }
}

void HeartsEngine::StartGame() {
    int highestScore = 0;
    while (highestScore < MAX_SCORE) {
        HeartsGame game(players, passScheme);
        game.Begin();
        
        for (HeartsPlayer* player : players) {
            scores[player] += game.Score(player);
            std::cout << "Score: " << scores[player] << "\n";
        }
        
        highestScore = 0;
        for (auto it = scores.begin(); it != scores.end(); ++it) {
            if (it->second > highestScore) {
                highestScore = it->second;
            }
        }
        std::cout << "highestScore: " << highestScore << "\n";
        
        passScheme = NextPassScheme(passScheme);
    }
}

HeartsEngine::PassScheme HeartsEngine::NextPassScheme(const PassScheme passScheme) const {
    switch (passScheme) {
        case PassScheme::LEFT:
            return PassScheme::RIGHT;
        case PassScheme::RIGHT:
            return PassScheme::ACROSS;
        case PassScheme::ACROSS:
            return PassScheme::NONE;
        case PassScheme::NONE:
            return PassScheme::LEFT;
    }
}