//
//  Deck.h
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/9/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#ifndef __Hearts__Deck__
#define __Hearts__Deck__

#include <vector>

#include "PlayingCard.h"

class Deck {
public:
    Deck();
    
    PlayingCard GetRandomCard();
    int Size() const;
private:
    std::vector<PlayingCard> deck;
};

#endif /* defined(__Hearts__Deck__) */
