//
//  Random.cpp
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/9/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#include "Random.h"

#include <random>
#include <chrono>
#include <iostream>

Random::RNG Random::engine;

bool Random::seeded = false;

Random::RNG& Random::Generator(){
    if(!seeded){
        num_type seed = static_cast<num_type>(std::chrono::system_clock::now().time_since_epoch().count());
        seeded = true;
        engine.seed(seed);
    }
    return engine;
}

int Random::Number(int min, int max){
    std::uniform_int_distribution<> distribution(min,max);
    Random::RNG& algorithm = Generator();
    int value = distribution(algorithm);
    return value;
}
