//
//  HeartsGame.cpp
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/12/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#include <iostream>
#include <map>
//#include <utility>

#include "HeartsGame.h"
#include "Deck.h"
#include "HeartsTrick.h"

HeartsGame::HeartsGame(const std::vector<HeartsPlayer*>& players, HeartsEngine::PassScheme passScheme) :isFirstTrick(true), isHeartsBroken(false), players(players), passScheme(passScheme), tricks(), HEARTS_PENALTY(1), QUEEN_SPADES_PENALTY(13) {
    
}

void HeartsGame::Begin() {
    std::cout << "\n";
    std::cout << "Begin Game\n";
    Deck deck;
    for (HeartsPlayer* player : players) {
        std::vector<PlayingCard> hand;
        for (int i=0; i<13; i++) {
            hand.push_back(deck.GetRandomCard());
        }
        player->AddCards(hand);
    }
    
    for (HeartsPlayer* player : players) {
        player->PrintHand();
    }
    std::cout << "Passing...\n";
    
    PassCards(passScheme);
    
    for (HeartsPlayer* player : players) {
        player->PrintHand();
    }
    std::cout << "Passing Complete\n";
    std::cout << "\n";
    
    for (HeartsPlayer* player : players) {
        if(player->IsFirstPlayer()) {
            trickLeader = player;
        }
    }
    
    while (!trickLeader->IsHandEmpty()) {
        TakeTrick();
    }
}

int HeartsGame::Score(const HeartsPlayer* player) const {
    if (HasShotMoon(player)){
        return 0;
    }
    else {
        for(HeartsPlayer* other : players) {
            if (other != player && HasShotMoon(other)) {
                return 26;
            }
        }
        
        std::vector<PlayingCard> cards;
        for (HeartsTrick trick : tricks) {
            cards.push_back(trick.getCard(player));
        }
        
        int score = 0;
        for (PlayingCard card : cards) {
            if (card.Suit() == PlayingCard::Suit::HEARTS) {
                score += HEARTS_PENALTY;
            }
            if (card.Suit() == PlayingCard::Suit::SPADES && card.Rank() == PlayingCard::Rank::QUEEN) {
                score += QUEEN_SPADES_PENALTY;
            }
        }
        return score;
    }
}

bool HeartsGame::HasShotMoon(const HeartsPlayer* player) const {
    std::vector<PlayingCard> cards;
    for (HeartsTrick trick : tricks) {
        cards.push_back(trick.getCard(player));
    }
    
    int hearts = 0;
    bool hasQueenSpades = false;
    for (PlayingCard card : cards) {
        if (card.Suit() == PlayingCard::Suit::HEARTS) {
            hearts += 1;
        }
        if (card.Suit() == PlayingCard::Suit::SPADES && card.Rank() == PlayingCard::Rank::QUEEN) {
            hasQueenSpades = true;
        }
    }
    bool hasAllHearts = (hearts == 13);
    
    return hasAllHearts && hasQueenSpades;
}

void HeartsGame::PassCards(const HeartsEngine::PassScheme passScheme) {
    switch (passScheme) {
        case HeartsEngine::PassScheme::LEFT: {
            std::map<HeartsPlayer*, std::vector<PlayingCard>> pass;
            for (HeartsPlayer* player : players) {
                std::vector<PlayingCard> cards = player->PassCards();
                pass.insert(std::pair<HeartsPlayer*, std::vector<PlayingCard>>(player, cards));
            }
            for (auto pair : pass) {
                std::vector<PlayingCard> cards = pair.second;
                HeartsPlayer* recipient = NextPlayer(pair.first);
                recipient->AddCards(cards);
            }
            break;
        }
        case HeartsEngine::PassScheme::RIGHT: {
            std::map<HeartsPlayer*, std::vector<PlayingCard>> pass;
            for (HeartsPlayer* player : players) {
                std::vector<PlayingCard> cards = player->PassCards();
                pass.insert(std::pair<HeartsPlayer*, std::vector<PlayingCard>>(player, cards));
            }
            for (auto pair : pass) {
                std::vector<PlayingCard> cards = pair.second;
                HeartsPlayer* recipient = NextPlayer(NextPlayer(NextPlayer(pair.first)));
                recipient->AddCards(cards);
            }
            break;
        }
        case HeartsEngine::PassScheme::ACROSS: {
            std::map<HeartsPlayer*, std::vector<PlayingCard>> pass;
            for (HeartsPlayer* player : players) {
                std::vector<PlayingCard> cards = player->PassCards();
                pass.insert(std::pair<HeartsPlayer*, std::vector<PlayingCard>>(player, cards));
            }
            for (auto pair : pass) {
                std::vector<PlayingCard> cards = pair.second;
                HeartsPlayer* recipient = NextPlayer(NextPlayer(pair.first));
                recipient->AddCards(cards);
            }
            break;
        }
        case HeartsEngine::PassScheme::NONE: {
            break;
        }
    }
}

void HeartsGame::TakeTrick() {
    HeartsTrick trick(isHeartsBroken, isFirstTrick);
    HeartsPlayer* player = trickLeader;
    while (!trick.IsCompleted()) {
        player->PrintHand();
        
        PlayingCard card = player->PromptCard();
        while (!trick.IsValid(card, player->Hand())) {
            std::vector<PlayingCard> cards;
            cards.push_back(card);
            player->AddCards(cards);
            card = player->PromptCard();
            //            std::cout << "Card:" << card.ToString() << "\n";
        }
        
        trick.AddCard(card, player);
        player = NextPlayer(player);
    }
    std::cout << "\n";
    
    isHeartsBroken = trick.IsHeartsBroken();
    trickLeader = trick.Winner();
    if (isFirstTrick) {
        isFirstTrick = false;
    }
    
    tricks.push_back(trick);
    
    trick.Print();
}

HeartsPlayer* HeartsGame::NextPlayer(const HeartsPlayer* player) const {
    std::vector<HeartsPlayer* const>::iterator index;
    for (auto it = players.begin(); it != players.end(); ++it) {
        if (*it == player) {
            index = ++it;
            break;
        }
    }
    
    if (index == players.end()) {
        index = players.begin();
    }
    
    return *index;
}


