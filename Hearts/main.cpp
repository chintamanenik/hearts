//
//  main.cpp
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/7/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//


#include "PlayingCard.h"
#include "Deck.h"
#include <iostream>
#include "HeartsEngine.h"

int main(int argc, const char * argv[]) {
    HeartsPlayer player1;
    HeartsPlayer player2;
    HeartsPlayer player3;
    HeartsPlayer player4;
    
    std::vector<HeartsPlayer*> players;
    players.push_back(&player1);
    players.push_back(&player2);
    players.push_back(&player3);
    players.push_back(&player4);
    
    for (int i=0; i<1; ++i) {
        HeartsEngine engine(players);
        engine.StartGame();
    }

    return 0;
}




