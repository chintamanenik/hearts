//
//  HeartsEngine.h
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/12/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#ifndef __Hearts__HeartsEngine__
#define __Hearts__HeartsEngine__

#include <vector>
#include <map>

#include "HeartsPlayer.h"

class HeartsEngine {
public:
    enum class PassScheme {LEFT, RIGHT, ACROSS, NONE};
    
    HeartsEngine(const std::vector<HeartsPlayer*>& players);
    
    void StartGame();
    
private:
    PassScheme NextPassScheme(const PassScheme passScheme) const;
    
    PassScheme passScheme;
    std::vector<HeartsPlayer*> players;
    std::map<HeartsPlayer*, int> scores;
    
    const int MAX_SCORE;
};

#endif /* defined(__Hearts__HeartsEngine__) */
