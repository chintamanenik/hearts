//
//  HeartsTrick.cpp
//  Hearts
//
//  Created by Kundan Chintamaneni on 2/14/15.
//  Copyright (c) 2015 Kundan Chintamaneni. All rights reserved.
//

#include <iostream>

#include "HeartsTrick.h"
#include "PlayingCard.h"

const int HeartsTrick::NUM_PLAYERS = 4;

HeartsTrick::HeartsTrick(bool isHeartsBroken, bool isFirstTrick) : isHeartsBroken(isHeartsBroken), isFirstTrick(isFirstTrick), cards(), players() {
    
}

bool HeartsTrick::IsCompleted() const {
    return cards.size() == NUM_PLAYERS;
}

bool HeartsTrick::IsHeartsBroken() const {
    return isHeartsBroken;
}

PlayingCard HeartsTrick::getCard(const HeartsPlayer* player) const {
    auto itc = cards.begin();
    for (auto itp = players.begin(); itp != players.end(); ++itp, ++itc) {
        if (*itp == player) {
            return *itc;
        }
    }
    return *itc; //Unreachable code; Player not found
}


void HeartsTrick::Print() {
    for (PlayingCard card : cards){
        std::cout << card.ToString();
    }
    std::cout << "\n";
}

HeartsPlayer* HeartsTrick::Winner() const {
    HeartsPlayer* winner = players.front();
    PlayingCard bestCard = cards.front();
    {
        auto itp = players.begin();
        for (auto itc = cards.begin(); itc != cards.end(); ++itc, ++itp) {
            bool isOnSuit = (itc->Suit() == baseSuit);
            bool isHigher = RankValue(itc->Rank()) > RankValue(bestCard.Rank());
            if (isOnSuit && isHigher) {
                winner = *itp;
                bestCard = *itc;
            }
        }
    }
//    bestCard.Print();
    return winner;
}

void HeartsTrick::AddCard(const PlayingCard& card, HeartsPlayer* player) {
    bool isFirstCard = cards.empty();
    if (isFirstCard) {
        baseSuit = card.Suit();
    }
    bool isHearts = card.Suit() == PlayingCard::Suit::HEARTS;
    if (!isHeartsBroken && isHearts) {
        isHeartsBroken = true;
    }
    cards.push_back(card);
    players.push_back(player);
}

bool HeartsTrick::IsValid(const PlayingCard& card, const std::vector<PlayingCard>& hand) const {
    bool isFirstCard = cards.empty();
    if (isFirstCard) {
        if (isFirstTrick) {
            bool isTwoClubs = (card.Suit() == PlayingCard::Suit::CLUBS) && (card.Rank() == PlayingCard::Rank::TWO);
            return isTwoClubs;
        }
        else {
            bool isHeart = (card.Suit() == PlayingCard::Suit::HEARTS);
            bool hasOnlyHearts = true;
            for (PlayingCard inHand : hand) {
                if (inHand.Suit() != PlayingCard::Suit::HEARTS) {
                    hasOnlyHearts = false;
                    break;
                }
            }
            if (!isHeartsBroken && isHeart && !hasOnlyHearts) {
                return false;
            }
            return true;
        }
    }
    else {
        if (isFirstTrick) {
            bool isHeart = (card.Suit() == PlayingCard::Suit::HEARTS);
            bool isQueenSpades = (card.Suit() == PlayingCard::Suit::HEARTS) && (card.Rank() == PlayingCard::Rank::QUEEN);
            bool isPoint = (isHeart || isQueenSpades);
            
            bool hasOnlyPoints = true;
            for (PlayingCard inHand : hand) {
                bool isHeart = (inHand.Suit() == PlayingCard::Suit::HEARTS);
                bool isQueenSpades = (inHand.Suit() == PlayingCard::Suit::HEARTS) && (inHand.Rank() == PlayingCard::Rank::QUEEN);
                if (!isHeart && !isQueenSpades) {
                    hasOnlyPoints = false;
                    break;
                }
            }
            
            if (isPoint && !hasOnlyPoints) {
                return false;
            }
            
            bool hasBaseSuit = false;
            for (PlayingCard inHand : hand) {
                if (inHand.Suit() == baseSuit) {
                    hasBaseSuit = true;
                    break;
                }
            }
            
            bool isOnSuit = (card.Suit() == baseSuit);
            if (hasBaseSuit && !isOnSuit) {
                return false;
            }
            return true;
        }
        else {
            bool hasBaseSuit = false;
            for (PlayingCard inHand : hand) {
                if (inHand.Suit() == baseSuit) {
                    hasBaseSuit = true;
                    break;
                }
            }
            
            bool isOnSuit = (card.Suit() == baseSuit);
            if (hasBaseSuit && !isOnSuit) {
                return false;
            }
            return true;
        }
    }
    
}

int HeartsTrick::RankValue(const enum PlayingCard::Rank rank) {
    switch (rank) {
        case PlayingCard::Rank::TWO:
            return 2;
        case PlayingCard::Rank::THREE:
            return 3;
        case PlayingCard::Rank::FOUR:
            return 4;
        case PlayingCard::Rank::FIVE:
            return 5;
        case PlayingCard::Rank::SIX:
            return 6;
        case PlayingCard::Rank::SEVEN:
            return 7;
        case PlayingCard::Rank::EIGHT:
            return 8;
        case PlayingCard::Rank::NINE:
            return 9;
        case PlayingCard::Rank::TEN:
            return 10;
        case PlayingCard::Rank::JACK:
            return 11;
        case PlayingCard::Rank::QUEEN:
            return 12;
        case PlayingCard::Rank::KING:
            return 13;
        case PlayingCard::Rank::ACE:
            return 14;
    }
}

