# README #

Hearts is a four player card game. This project will allow a user to play a game of Hearts with computer opponents. 

### What is this repository for? ###

* This repository is a private hosting. 

### How do I get set up? ###

* The project is currently in development and no minimal example can be run.

### Contribution guidelines ###

* Files merged to the master branch must be unit tested.

### Who do I talk to? ###

* Email all comments to omnik116@gmail.com